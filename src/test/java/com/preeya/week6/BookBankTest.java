package com.preeya.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess(){
        BookBank book = new BookBank("Preeya",100);
        book.withdrew(50);
        assertEquals(50,book.getBalance(),0.0001);
    }
    @Test
    public void shouldWithdrawOver(){
        BookBank book = new BookBank("Preeya",100);
        book.withdrew(150);
        assertEquals(100,book.getBalance(),0.0001);
    }
    @Test
    public void shouldWithdrawWithNegativeNumber(){
        BookBank book = new BookBank("Preeya",100);
        book.withdrew(-100);
        assertEquals(100,book.getBalance(),0.0001);
    }
}
