package com.preeya.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank preeya = new BookBank("Preeya",50.0);
        preeya.print();

        BookBank baitong = new BookBank("Baitong",100000.0);
        baitong.print();
        baitong.withdrew(40000.0);
        baitong.print();

        preeya.deposit(40000.0);
        preeya.print();

        BookBank buatong = new BookBank("Buatong",100000.0);
        buatong.print();
        buatong.deposit(2);
        buatong.print();
    }
}